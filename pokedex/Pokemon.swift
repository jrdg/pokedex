//
//  Pokemon.swift
//  pokedex
//
//  Created by Jordan Gauthier on 6/15/17.
//  Copyright © 2017 Jordan Gauthier. All rights reserved.
//

import Foundation

class Pokemon{
    
    fileprivate var _name : String!
    fileprivate var _pokedexId : Int!
    
    
    init(name : String , pokedexId : Int) {
        _name = name
        _pokedexId = pokedexId
    }
    
    var name : String{
        get{
            return _name
        }
        
        set{
            _name = newValue
        }
    }
    
    var pokedexId : Int {
        get{
            return _pokedexId
        }
        
        set{
            _pokedexId = newValue
        }
    }
    
}
