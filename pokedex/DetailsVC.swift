//
//  DetailsVC.swift
//  pokedex
//
//  Created by Jordan Gauthier on 6/15/17.
//  Copyright © 2017 Jordan Gauthier. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {
    
    var name : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(name)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
