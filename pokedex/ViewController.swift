//
//  ViewController.swift
//  pokedex
//
//  Created by Jordan Gauthier on 6/15/17.
//  Copyright © 2017 Jordan Gauthier. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout ,UISearchBarDelegate {

    @IBOutlet weak var _collectionView: UICollectionView!
    @IBOutlet weak var _searchBar: UISearchBar!
    
    var pokemons : [Pokemon]!  // array qui a tous les pokemon (ses le array par default)
    var filteredPokemons : [Pokemon]! // array qui contiendra tous les pokemon retourner par la recherche
    var isInSearchMode : Bool! //bool qui determine si on es dans une recherche ou non
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        pokemons = [Pokemon]()
        filteredPokemons = [Pokemon]()
        isInSearchMode = false;
        
        
        //let charmander = Pokemon(name: "Charmander", pokedexId: 4)
        
        _collectionView.delegate = self
        _collectionView.dataSource = self
        _collectionView.backgroundColor = UIColor.clear
        _searchBar.delegate = self
        _searchBar.enablesReturnKeyAutomatically = false
        self.parsePokemonFromCsv()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = _collectionView.dequeueReusableCell(withReuseIdentifier: "PokeCell", for: indexPath) as? PokeCell{
            let pokemon : Pokemon!
            
            if isInSearchMode{
                pokemon = filteredPokemons[indexPath.row]
                cell.updateUI(pokemon)
            }else{
                pokemon = pokemons[indexPath.row]
                cell.updateUI(pokemon)
            }
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isInSearchMode{
            return filteredPokemons.count
        }else{
            return pokemons.count
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text == nil || searchBar.text == ""{
            isInSearchMode = false
            _collectionView.reloadData()
        }else{
            isInSearchMode = true
            
            let text = searchBar.text!.lowercased()
            
            filteredPokemons = pokemons.filter({
                $0.name.range(of: text) != nil
                //$0 = placeholder iterate threw pokemons array
            })
            
            _collectionView.reloadData()
            
        }
        
        view.endEditing(true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = _collectionView.cellForItem(at: indexPath) as? PokeCell{
            performSegue(withIdentifier: "DetailsVC", sender: cell)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? DetailsVC{
            
            if let s = sender as? PokeCell{
                 destination.name = s._pokemonLabel.text!
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 105, height: 105)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func parsePokemonFromCsv(){
        let path = Bundle.main.path(forResource: "pokemon", ofType: "csv")
        
        do{
            let csv = try CSV(contentsOfURL: path!)
            let rows = csv.rows
            print(rows)
            
            for row in rows{
                
                var pokeId : Int?
                var pokeName : String?
                
                if let id = row["id"]{
                    pokeId = Int(id)
                }
                
                if let name = row["identifier"]{
                    pokeName = name
                }
                
                if  !(pokeName == nil && pokeId == nil){
                    let pokemon = Pokemon(name: pokeName!, pokedexId: pokeId!)
                    pokemons.append(pokemon)
                }
            }
            
        }catch let err as NSError{
            print(err.debugDescription)
        }
    }
}

