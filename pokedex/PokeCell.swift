//
//  PokeCell.swift
//  pokedex
//
//  Created by Jordan Gauthier on 6/15/17.
//  Copyright © 2017 Jordan Gauthier. All rights reserved.
//

import UIKit

class PokeCell: UICollectionViewCell {
    
    @IBOutlet weak var _pokemonImg: UIImageView!
    @IBOutlet weak var _pokemonLabel: UILabel!
    
    func updateUI(_ pokemon : Pokemon){
        _pokemonLabel.text = pokemon.name.capitalized
        _pokemonImg.image = UIImage(named: "\(pokemon.pokedexId)")
        self.layer.cornerRadius = 5.0
    }
    
}
